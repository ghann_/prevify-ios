//
//  background.swift
//  prevify
//
//  Created by unrealBots on 6/1/17.
//  Copyright © 2017 unrealBots. All rights reserved.
//

import Foundation
import SystemConfiguration
import UIKit
import SparkImageView

extension UIView{
    func addBackground(imageName: String = "background.jpg", contextMode: UIViewContentMode = .scaleToFill) {
        // setup the UIImageView
        let backgroundImageView = UIImageView(frame: UIScreen.main.bounds)
        backgroundImageView.image = UIImage(named: imageName)
        backgroundImageView.contentMode = contentMode
        backgroundImageView.translatesAutoresizingMaskIntoConstraints = false
        
        addSubview(backgroundImageView)
        sendSubview(toBack: backgroundImageView)
        
        // adding NSLayoutConstraints
        let leadingConstraint = NSLayoutConstraint(item: backgroundImageView, attribute: .leading, relatedBy: .equal, toItem: self, attribute: .leading, multiplier: 1.0, constant: 0.0)
        let trailingConstraint = NSLayoutConstraint(item: backgroundImageView, attribute: .trailing, relatedBy: .equal, toItem: self, attribute: .trailing, multiplier: 1.0, constant: 0.0)
        let topConstraint = NSLayoutConstraint(item: backgroundImageView, attribute: .top, relatedBy: .equal, toItem: self, attribute: .top, multiplier: 1.0, constant: 0.0)
        let bottomConstraint = NSLayoutConstraint(item: backgroundImageView, attribute: .bottom, relatedBy: .equal, toItem: self, attribute: .bottom, multiplier: 1.0, constant: 0.0)
        
        NSLayoutConstraint.activate([leadingConstraint, trailingConstraint, topConstraint, bottomConstraint])
    }
    func rotate360Degrees(duration: CFTimeInterval = 25) {
        let rotateAnimation = CABasicAnimation(keyPath: "transform.rotation")
        rotateAnimation.fromValue = 0.0
        rotateAnimation.toValue = CGFloat(Double.pi * 2)
        rotateAnimation.isRemovedOnCompletion = false
        rotateAnimation.duration = duration
        rotateAnimation.repeatCount=Float.infinity
        self.layer.add(rotateAnimation, forKey: nil)
    }
    
    func fadeIn(after delay: Double = 0){
        UIView.animate(withDuration: 0, delay: 0, options: .curveEaseInOut, animations: {
            self.alpha = 0.0
        }, completion: { _ in
            UIView.animate(withDuration: 1, delay: delay, animations: {
                self.alpha = 1.0
            })
        })
    }
    
    func logoFadeIn(handler: @escaping ()->Void){
        UIView.animate(withDuration: 0, delay: 0, options: .curveEaseInOut, animations: {
            self.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
            self.alpha = 0.0
        }, completion: { _ in
            UIView.animate(withDuration: 1.1, delay: 0, options: .curveEaseInOut, animations: {
                self.transform = CGAffineTransform(scaleX: 1.9, y: 1.9)
                self.alpha = 1.0
            }, completion: { _ in
                UIView.animate(withDuration: 0.25, delay: 0, options: .curveEaseInOut, animations: {
                    self.transform = CGAffineTransform(scaleX: 1.7, y: 1.7)
                }, completion: { _ in handler()})
            })
        })
    }
    
    func popRotatingImage(){
        self.transform = CGAffineTransform(scaleX: 0.0, y: 0.0)
        UIView.animate(withDuration: 0.8, delay: 0, options: .curveEaseInOut, animations: {
            self.transform = CGAffineTransform(scaleX: 1.15, y: 1.15)
        }) { _ in
            guard let spark = self as? SparkImageView else{return}
            spark.animate(withDuration: 0.75, radius: 1.1, completion: nil)
            UIView.animate(withDuration: 0.4, delay: 0, options: .curveEaseInOut, animations: {
                self.transform = CGAffineTransform.identity
            }, completion: nil)
        }
    }
    
    func popFadeIn(handler: (()->Void)? = nil){
        UIView.animate(withDuration: 0.25, delay: 0, options: .curveEaseInOut, animations: {
            self.transform = CGAffineTransform(scaleX: 1.1, y: 1.1)
            self.alpha = 0.75
        }, completion: { _ in
            UIView.animate(withDuration: 0.25, delay: 0, options: .curveEaseInOut, animations: {
                self.transform = CGAffineTransform.identity
                self.alpha = 1
            }, completion: { _ in
                handler?()
            })
        })
    }
    
    func applyMotionEffect(magnitude: CGFloat){
        let xMotion = UIInterpolatingMotionEffect(keyPath: "center.x", type: .tiltAlongHorizontalAxis)
        xMotion.minimumRelativeValue = -magnitude
        xMotion.maximumRelativeValue = magnitude
        let yMotion = UIInterpolatingMotionEffect(keyPath: "center.y", type: .tiltAlongVerticalAxis)
        yMotion.minimumRelativeValue = -magnitude
        yMotion.maximumRelativeValue = magnitude
        
        let group = UIMotionEffectGroup()
        group.motionEffects = [xMotion, yMotion]
        
        self.transform = CGAffineTransform(scaleX: 1.1, y: 1.1)
        self.addMotionEffect(group)
    }
}

extension UIViewController{
    func showDefaultAlert(alertTitle title: String, alertMessage message: String,
                          okText ok: String = "Ok", cancelText cancel: String = "Cancel",
                          okHandler handler: (()->Void)? = nil,
                          cancelHandler chandler: (()->Void)? = nil){
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: ok, style: .default, handler: { _ in handler?() }))
        alert.addAction(UIAlertAction(title: cancel, style: .default, handler: { _ in chandler?() }))
        present(alert, animated: true, completion: nil)
    }
    
    func showAlertWithOkOnly(alertTitle title: String, alertMessage message: String,
                             okText ok: String = "Ok", okHandler handler: (() -> Void)? = nil){
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: ok, style: .default, handler: { _ in handler?() }))
        present(alert, animated: true, completion: nil)
    }
    
    func showActionSheet(title: String, message: String, firstActionTitle: String, firstHandler: @escaping ()->Void, secondActionTitle: String, secondHandler: @escaping ()->Void, thirdActionTitle: String, thirdHandler: @escaping ()->Void){
        let actionSheet = UIAlertController(title: title, message: message, preferredStyle: .actionSheet)
        actionSheet.addAction(UIAlertAction(title: firstActionTitle, style: .default, handler: { (_) in
            firstHandler()
        }))
        actionSheet.addAction(UIAlertAction(title: secondActionTitle, style: .default, handler: { (_) in
            secondHandler()
        }))
        actionSheet.addAction(UIAlertAction(title: thirdActionTitle, style: .default, handler: { (_) in
            thirdHandler()
        }))
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        actionSheet.view.tintColor = UIColor(red:0.20, green:0.20, blue:0.20, alpha:1.0)
        present(actionSheet, animated: true, completion: nil)
    }
    
    func setUpNavigation(completion: (UIButton, UIImageView) -> ()){
        let leftMenuButton = UIButton.init(type: .custom)
        leftMenuButton.setImage(UIImage(named: "menu"), for: .normal)
        leftMenuButton.setImage(UIImage(named: "menu-pressed"), for: .highlighted)
        leftMenuButton.frame = CGRect(x: 0, y: 0, width: 24, height: 24)
        
        let centerLogoView = UIImageView(frame: CGRect(x: 0, y: 0, width: 80, height: 28))
        centerLogoView.contentMode = .scaleAspectFit
        let centerLogo = UIImage(named: "logo-center.png")
        
        leftMenuButton.widthAnchor.constraint(equalToConstant: 28).isActive = true
        leftMenuButton.heightAnchor.constraint(equalToConstant: 28).isActive = true
        centerLogoView.heightAnchor.constraint(equalToConstant: 28).isActive = true

        centerLogoView.image = centerLogo
        
        completion(leftMenuButton, centerLogoView)
    }
    
    func setUpNavigationForLiked(completion: (UIButton, UIImageView, UIButton) -> ()){
        let leftMenuButton = UIButton.init(type: .custom)
        leftMenuButton.setImage(UIImage(named: "menu"), for: .normal)
        leftMenuButton.setImage(UIImage(named: "menu-pressed"), for: .highlighted)
        leftMenuButton.frame = CGRect(x: 0, y: 0, width: 24, height: 24)
        
        let centerLogoView = UIImageView(frame: CGRect(x: 0, y: 0, width: 80, height: 28))
        centerLogoView.contentMode = .scaleAspectFit
        let centerLogo = UIImage(named: "logo-center.png")
        
        leftMenuButton.widthAnchor.constraint(equalToConstant: 28).isActive = true
        leftMenuButton.heightAnchor.constraint(equalToConstant: 28).isActive = true
        centerLogoView.heightAnchor.constraint(equalToConstant: 28).isActive = true
        
        centerLogoView.image = centerLogo
        
        let trashButton   = UIButton.init(type: .custom)
        trashButton.setImage(UIImage(named: "trash_navigation"), for: UIControlState.normal)
        trashButton.frame = CGRect(x: 0, y: 0, width: 24, height: 24)
        trashButton.widthAnchor.constraint(equalToConstant: 24).isActive = true
        trashButton.heightAnchor.constraint(equalToConstant: 24).isActive = true
       
        completion(leftMenuButton, centerLogoView, trashButton)
    }
}

extension UISearchBar{
    func configureSearchBar(){
        let textFieldInsideSearchBar = self.value(forKey: "searchField") as! UITextField
        textFieldInsideSearchBar.font = UIFont(name: "Montserrat-Light", size: 16)!
        textFieldInsideSearchBar.textColor = UIColor(red:0.47, green:0.88, blue:0.69, alpha:1.0)
    }
}
