//
//  API.swift
//  prevify
//
//  Created by unrealBots on 2/2/18.
//  Copyright © 2018 unrealBots. All rights reserved.
//

import Foundation
import UIKit
import OAuthSwift
import Alamofire
import SwiftyJSON

class API{
    static let shared = API()
    
    func doOAuth(from vc: UIViewController, handler: @escaping (Bool, String?, Error?)->Void){
        var oauthswift: OAuth2Swift?
        
        oauthswift = OAuth2Swift(
            consumerKey: Globals.consumerKey,
            consumerSecret: Globals.consumerSecret,
            authorizeUrl: Globals.authorizeUrl,
            accessTokenUrl: Globals.accessTokenUrl,
            responseType: "token"
        )
        
        oauthswift?.authorizeURLHandler = SafariURLHandler(viewController: vc, oauthSwift: oauthswift!)
        let state = generateState(withLength: 20)
        
        oauthswift?.authorize(
            withCallbackURL: URL(string: "prevify://")!,
            scope: "user-library-modify",
            state: state,
            success: { credential, response, parameters in
                handler(true, credential.oauthToken, nil)
        }, failure: { error in
            handler(false, nil, error)
        })
    }
    
    func logoutAuth(handler: ()->Void){
        URLCache.shared.removeAllCachedResponses()
        URLCache.shared.diskCapacity = 0
        URLCache.shared.memoryCapacity = 0
        
        let storage = HTTPCookieStorage.shared
        storage.cookies?.forEach() { storage.deleteCookie($0) }
        
        let domain = Bundle.main.bundleIdentifier!
        UserDefaults.standard.removePersistentDomain(forName: domain)
        UserDefaults.standard.synchronize()
        handler()
    }
    
    func openSpotify(with url: URL, from sender: UIViewController){
        if UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }else{
            sender.showAlertWithOkOnly(alertTitle: "Error", alertMessage: "Application not found")
        }
    }
    
    func login(with username: String, and password: String, handler: @escaping (Bool, String?, Error?)->Void){
        let parameters: Parameters = ["login":true, "name":username, "pass":password]
        Alamofire.request(Globals.URL.User, method: .post, parameters: parameters, encoding: URLEncoding.default).responseString { response in
            switch response.result{
            case .success(let responseCode):
                responseCode.count > 1 ? handler(true, responseCode, nil) : handler(false, "Invalid username or password", nil)
            case .failure(let error):
                handler(false, nil, error)
            }
        }
    }
    
    func register(with username:String, and password: String, handler: @escaping (Bool, Int?, Error?)->Void){
        let parameters: Parameters = ["name":username, "pass":password]
        Alamofire.request(Globals.URL.User, method: .post, parameters: parameters, encoding: URLEncoding.default).responseString { response in
            switch response.result{
            case .success(let responseCode):
                handler(true, Int(responseCode)!, nil)
            case .failure(let error):
                handler(false, nil, error)
            }
        }
    }
    
    func getNewReleases(handler: @escaping (Bool, Any?, Error?)->Void){
        Alamofire.request(Globals.homeUrlString, headers: Globals.headers).responseJSON(completionHandler: { response in
            switch response.result{
            case .success(let value):
                let json = JSON(value)
                let newReleases = json["albums"]["items"]
                var newReleasesCollections = [Song]()
                
                guard !json["error"].exists() else {
                    guard let webApiStatus = json["error"]["status"].int,
                        let webApiErr = json["error"]["message"].string else {return}
                    handler(false, "\(webApiStatus): \(webApiErr)", nil)
                    return
                }
                
                for i in 0..<newReleases.count{
                    guard let artistName = newReleases[i]["artists"][0]["name"].string,
                          let songName   = newReleases[i]["name"].string,
                          let url        = newReleases[i]["uri"].string,
                          let image      = newReleases[i]["images"][0]["url"].string,
                          let imageURL   = URL(string: image) else {continue}
                    
                    let newRelease = Song(songArtist: artistName, songImage: imageURL, songName: songName, spotifyURL: url)
                    newReleasesCollections.append(newRelease)
                }
                handler(true, newReleasesCollections, nil)
            case .failure(let error):
                handler(false, nil, error)
            }
        })
    }
    
    func searchTracks(keywords: String, handler: @escaping ((Bool, Any?, Error?)->Void)){
        let url = "https://api.spotify.com/v1/search?q=\(keywords)&type=track"
        Alamofire.request(url, headers: Globals.headers).responseJSON(completionHandler: { response in
            switch response.result{
            case .success(let value):
                let json = JSON(value)
                let songs = json["tracks"]["items"]
                var searchedSongs = [Song]()
                
                guard !json["error"].exists() else {
                    guard let webApiStatus = json["error"]["status"].int,
                          let webApiErr = json["error"]["message"].string else {return}
                    handler(false, "\(webApiStatus): \(webApiErr)", nil)
                    return
                }
                
                for i in 0..<songs.count{
                    guard let artistName = songs[i]["artists"][0]["name"].string,
                          let name       = songs[i]["name"].string,
                          let popularity = songs[i]["popularity"].int,
                          let id         = songs[i]["id"].string,
                          let previewURL = songs[i]["preview_url"].string,
                          let spotifyURL = songs[i]["artists"][0]["uri"].string,
                          let artistID   = songs[i]["artists"][0]["id"].string,
                          let songURLstr = songs[i]["uri"].string,
                          let songURL    = URL(string: songURLstr),
                          let image      = songs[i]["album"]["images"][0]["url"].string,
                          let imageURL   = URL(string: image) else {continue}

                    let newSong = Song(songId: id, songImage: imageURL, songName: name, songArtist: artistName,
                                       songPopularity: popularity, songPreviewURL: previewURL, spotifyURL: spotifyURL,
                                       artistID: artistID, songURL: songURL)
                    searchedSongs.append(newSong)
                }
                handler(true, searchedSongs, nil)
            case .failure(let error):
                handler(false, nil, error)
            }
        })
    }
    
    func saveToLikedList(songId id: String, songTitle title: String, handler: @escaping (Bool, String?, Error?)->Void){
        let parameters: Parameters = ["add" : true, "uid" : Globals.uid, "trackid" : id, "songName" : title]
        Alamofire.request(Globals.URL.LikedList, method: .post, parameters: parameters, encoding: URLEncoding.httpBody).responseString { response in
            switch response.result{
            case .success(let value):
                handler(true, value, nil)
            case .failure(let error):
                handler(false, nil, error)
            }
        }
    }
    
    func getLikedListTrackID(handler: @escaping (Bool, Any?, Error?)->Void){
        Alamofire.request("\(Globals.URL.LikedList)?GETuid=\(Globals.uid)", method: .get).responseJSON { response in
            switch response.result{
            case .success(let value):
                let trackIDs = JSON(value)
                var trackIDCollection = [String]()
            
                for i in 0..<trackIDs.count{
                    guard let trackID = trackIDs[i]["trackId"].string else {continue}
                    trackIDCollection.append(trackID)
                }
                handler(true, trackIDCollection, nil)
            case .failure(let error):
                handler(false, nil, error)
            }
        }
    }
    
    func getLikedList(trackIds: [String], handler: @escaping (Bool, [Song]?, Error?)->Void){
        var songs = [Song]()
        let dispatchGroup = DispatchGroup()
        
        for i in 0..<trackIds.count{
            dispatchGroup.enter()
            let searchURLTracks = "https://api.spotify.com/v1/tracks/\(trackIds[i])"
            Alamofire.request(searchURLTracks, headers: Globals.headers).responseJSON(completionHandler: { response in
                switch response.result{
                case .success(let value):
                    let json = JSON(value)
                    guard let artistName = json["artists"][0]["name"].string,
                          let name       = json["name"].string,
                          let popularity = json["popularity"].int,
                          let id         = json["id"].string,
                          let previewURL = json["preview_url"].string,
                          let spotifyURL = json["artists"][0]["uri"].string,
                          let artistID   = json["artists"][0]["id"].string,
                          let songURLstr = json["uri"].string,
                          let songURL    = URL(string: songURLstr),
                          let image      = json["album"]["images"][0]["url"].string,
                          let imageURL   = URL(string: image) else {return}
                    
                    let song = Song(songId: id, songImage: imageURL, songName: name, songArtist: artistName,
                                    songPopularity: popularity, songPreviewURL: previewURL, spotifyURL: spotifyURL,
                                    artistID: artistID, songURL: songURL)
                    songs.append(song)
                    dispatchGroup.leave()
                case .failure(let error):
                    handler(false, nil, error)
                }
            })
        }
        dispatchGroup.notify(queue: .main) {
            handler(true, songs, nil)
        }
    }
    
    func removeAllLikedList(handler: @escaping (Bool, Any?, Error?)->Void){
        let parameters: Parameters = ["truncate":true, "uid":Globals.uid]
        Alamofire.request(Globals.URL.LikedList, method: .post, parameters: parameters, encoding: URLEncoding.default).responseString { response in
            switch response.result{
            case .success(let value):
                handler(true, value, nil)
            case .failure(let error):
                handler(false, nil, error)
            }
        }
    }
    
    func removeTracks(song: Song, handler: @escaping (Bool, Any?, Error?)->Void){
        let parameters: Parameters = ["delete":true, "uid":Globals.uid, "trackid":song.id, "songName":song.name]
        Alamofire.request(Globals.URL.LikedList, method: .post, parameters: parameters, encoding: URLEncoding.default).responseString { response in
            switch response.result{
            case .success(let value):
                handler(true, value, nil)
            case .failure(let error):
                handler(false, nil, error)
            }
        }
    }
    
    func getDetailArtist(artistID id: String, handler: @escaping (Bool, Any?, Error?)->Void){
        let url = "https://api.spotify.com/v1/artists/\(id)"
        Alamofire.request(url, headers: Globals.headers).responseJSON(completionHandler: { response in
            switch response.result{
            case .success(let value):
                let json = JSON(value)
                guard let artistName  = json["name"].string,
                      let popularity  = json["popularity"].int,
                      let url         = json["uri"].string,
                      let followers   = json["followers"]["total"].int,
                      let image       = json["images"][0]["url"].string,
                      let imageURL    = URL(string: image) else {return}
                
                var genres = [String]()
                guard let arrGenre = json["genres"].array else {return}
                for i in 0..<arrGenre.count{
                    guard let gnr = arrGenre[i].string else {return}
                    genres.append(gnr)
                }
                
                let artist = Artist(artistName: artistName, artistImage: imageURL,
                                    artistPopularity: popularity, genres: genres,
                                    totalFollowers: followers, spotifyURL: url)
                handler(true, artist, nil)
            case .failure(let error):
                handler(false, nil, error)
            }
        })
    }
    
    func getTopTracksFromArtist(artistID id: String, handler: @escaping (Bool, Any?, Error?)->Void){
        let url = "https://api.spotify.com/v1/artists/\(id)/top-tracks?country=US"
        Alamofire.request(url, headers: Globals.headers).responseJSON { response in
            switch response.result{
            case .success(let value):
                let json = JSON(value)
                let tracks = json["tracks"]
                var topTracks = [Song]()
                for i in 0..<tracks.count{
                    guard let songName = tracks[i]["name"].string,
                          let image    = tracks[i]["album"]["images"][0]["url"].string,
                          let imageURL = URL(string: image) else {continue}
                    
                    let tracks = Song(songName: songName, songImage: imageURL)
                    topTracks.append(tracks)
                }
                handler(true, topTracks, nil)
            case .failure(let err):
                handler(false, nil, err)
            }
        }
    }
}
