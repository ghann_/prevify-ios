//
//  Globals.swift
//  prevify
//
//  Created by unrealBots on 10/24/17.
//  Copyright © 2017 unrealBots. All rights reserved.
//

import Foundation
import UIKit
import Alamofire

class Globals {
    static let appDelegate = UIApplication.shared.delegate as? AppDelegate
    static let defaults: UserDefaults = UserDefaults.standard
    static let uid = Globals.defaults.string(forKey: "uid")!
    
    static let consumerKey = "e13421adc8fc41ffa90964089c755282"
    static let consumerSecret = "84fe4d68f8ac4a09b7851a802a251341"
    static let authorizeUrl = "https://accounts.spotify.com/en/authorize"
    static let accessTokenUrl = "https://accounts.spotify.com/api/token"
    
    static let leftMenuItem = ["Home", "Preview Song", "Liked List", "About", "Log Out"]
    static let leftMenuIcon = ["home", "preview", "liked", "about", "logout"]
    
    static let homeUrlString = "https://api.spotify.com/v1/browse/new-releases?limit=20"
    static var headers: HTTPHeaders {
        let token = UserDefaults.standard.string(forKey: "token")!
        return [
            "Authorization" : "Bearer \(token)",
            "Contenct-Type" : "application/json"
        ]
    }
    
    class URL {
        static let LikedList = "http://prevify.unrealbots.net/likedlist.php"
        static let User = "http://prevify.unrealbots.net/user.php"
    }
    
    enum Shortcut: String{
        case Search = "Search"
        case Liked = "Liked"
    }
}
