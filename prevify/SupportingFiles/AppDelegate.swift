//
//  AppDelegate.swift
//  prevify
//
//  Created by unrealBots on 5/31/17.
//  Copyright © 2017 unrealBots. All rights reserved.
//

import UIKit
import OAuthSwift
import SVProgressHUD

@UIApplicationMain 
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        OAuth2Swift.handle(url: url)
        UIApplication.shared.keyWindow?.rootViewController?.dismiss(animated: true, completion: nil)
        return true
    }

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        self.window = UIWindow(frame: UIScreen.main.bounds)
        configureNavigation()
        configureSVProgressHUD()
    
        let loggedIn = Globals.defaults.bool(forKey: "isLoggedIn")
        if loggedIn {
            let storyboard = UIStoryboard(name: "LeftMenu", bundle: nil)
            let homeVC = storyboard.instantiateInitialViewController()
            
            self.window?.rootViewController = homeVC
        }else{
            let storyboard = UIStoryboard(name: "LoginSignUp", bundle: nil)
            let SignInVC = storyboard.instantiateInitialViewController()
            
            self.window?.rootViewController = SignInVC
        }
        self.window?.makeKeyAndVisible()
        
        return true
    }
    func configureNavigation(){
        let textAttributes      = [NSAttributedStringKey.foregroundColor : UIColor.white,
                                   NSAttributedStringKey.font: UIFont(name: "Montserrat-SemiBold", size: 18)!]
        UINavigationBar.appearance().titleTextAttributes = textAttributes
        UINavigationBar.appearance().tintColor = UIColor("#ffffff")
        UIBarButtonItem.appearance().setTitleTextAttributes([NSAttributedStringKey.foregroundColor: UIColor.clear], for: .normal)
    }
    func configureSVProgressHUD(){
        SVProgressHUD.setBackgroundColor(UIColor(red:0.67, green:0.88, blue:0.78, alpha:1.0))
        SVProgressHUD.setForegroundColor(UIColor(red:1.0, green:1.0, blue:1.0, alpha:1.0))
        SVProgressHUD.setFont(UIFont(name: "Montserrat", size: 15)!)
        SVProgressHUD.setRingThickness(2.5)
    }

    func application(_ application: UIApplication, performActionFor shortcutItem: UIApplicationShortcutItem, completionHandler: @escaping (Bool) -> Void) {
        completionHandler(handleQuickAction(shortcutItem: shortcutItem))
    }
    
    func handleQuickAction(shortcutItem: UIApplicationShortcutItem) -> Bool {
        var quickActionHandled = false
        let type = shortcutItem.type.components(separatedBy: ".").last!
        if let shortcutType = Globals.Shortcut(rawValue: type){
            guard let vc = UIStoryboard(name: "LeftMenu", bundle: nil).instantiateInitialViewController() as? ContainerViewController else { return false}
            vc.shortcut = shortcutType
            self.window?.rootViewController = vc
            self.window?.makeKeyAndVisible()
            quickActionHandled = true
        }
        
        return quickActionHandled
    }
}

