//
//  AudioPlayer.swift
//  prevify
//
//  Created by unrealBots on 2/3/18.
//  Copyright © 2018 unrealBots. All rights reserved.
//

import Foundation
import AVFoundation

class AudioPlayer: NSObject{
    static let shared = AudioPlayer()
    weak var delegate: AudioPlayerDelegate? = nil
    
    var player = AVAudioPlayer()
    var isPlaying: Bool{
        get{ return player.isPlaying }
    }

    func play(){
        player.play()
    }
    func pause(){
        player.pause()
    }
    
    func playPreview(of url: URL){
        do{
            player = try AVAudioPlayer(contentsOf: url)
            player.delegate = self
            player.prepareToPlay()
            player.numberOfLoops = 0
            player.play()
        }catch{
            print("Error: \(error.localizedDescription)")
        }
    }
}

extension AudioPlayer: AVAudioPlayerDelegate{
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
        delegate?.didFinishedPlayMusic()
    }
}
