//
//  Protocols.swift
//  prevify
//
//  Created by unrealBots on 2/28/18.
//  Copyright © 2018 unrealBots. All rights reserved.
//

import Foundation
import Kingfisher
import UIKit

protocol ImageCacheable{
    func downloadImage(from url: URL, to imageView: UIImageView, fadeIn: TimeInterval)
}

extension ImageCacheable{
    func downloadImage(from url: URL, to imageView: UIImageView, fadeIn: TimeInterval = 0.5){
        imageView.kf.indicatorType = .activity
        imageView.kf.setImage(with: url, options: [.transition(.fade(fadeIn))])
    }
}

protocol AudioPlayerDelegate: class {
    func didFinishedPlayMusic()
}

protocol TableViewEmptiable{
    func configureTableView(tableView: UITableView)
    func configureEmptyTableView(tableView: UITableView, emptyText: String)
}

extension TableViewEmptiable{
    func configureTableView(tableView: UITableView){
        tableView.isScrollEnabled = true
        tableView.separatorStyle  = .singleLine
        tableView.backgroundView  = nil
        tableView.backgroundColor = UIColor("#f9fffb")
    }
    func configureEmptyTableView(tableView: UITableView, emptyText: String){
        let noDataLabel: UILabel    = UILabel(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: tableView.bounds.size.height))
        noDataLabel.text            = emptyText
        noDataLabel.textColor       = UIColor(red:0.47, green:0.88, blue:0.69, alpha:1.0)
        noDataLabel.backgroundColor = UIColor(red:0.98, green:1.00, blue:0.98, alpha:1.0)
        noDataLabel.textAlignment   = .center
        noDataLabel.font            = UIFont(name: "Montserrat-Light", size: 18)
        tableView.isScrollEnabled   = false
        tableView.backgroundView    = noDataLabel
        tableView.separatorStyle    = .none
    }
}

protocol TableViewAnimateable{
    func animateTable(table: UITableView)
}

extension TableViewAnimateable{
    func animateTable(table: UITableView){
        let cells = table.visibleCells
        let tableHeight = table.bounds.size.height
        var ctr = 0
        
        _ = cells.map{ $0.transform = CGAffineTransform(translationX: 0, y: tableHeight) }
        for cell in cells{
            UIView.animate(withDuration: 1.15, delay: Double(ctr) * 0.05, usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: .curveEaseInOut, animations: {
                 cell.transform = CGAffineTransform.identity
            }, completion: nil)
            ctr += 1
        }
    }
}
