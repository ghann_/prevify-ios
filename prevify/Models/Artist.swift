//
//  Artist.swift
//  prevify
//
//  Created by unrealBots on 2/7/18.
//  Copyright © 2018 unrealBots. All rights reserved.
//

import Foundation
import UIKit

class Artist{
    let name: String
    let image: URL
    let popularity: Int
    let spotifyURL: String
    fileprivate let genres: [String]
    fileprivate let totalFollowers: Int
    
    var genre: String{
        return genres.count > 3 ? genres.prefix(3).joined(separator: ", ") : genres.joined(separator: ", ")
    }
    var followers: String{
        let formatter = NumberFormatter()
        formatter.numberStyle = .decimal
        guard let number = formatter.string(from: NSNumber(value:totalFollowers)) else {
            return String(totalFollowers)
        }
        return number
    }
    
    init(artistName name: String, artistImage image: URL, artistPopularity popularity: Int,
         genres: [String], totalFollowers: Int, spotifyURL: String) {
        self.name = name
        self.image = image
        self.popularity = popularity
        self.genres = genres
        self.totalFollowers = totalFollowers
        self.spotifyURL = spotifyURL
    }
}
