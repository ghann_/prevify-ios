//
//  Song.swift
//  prevify
//
//  Created by unrealBots on 10/30/17.
//  Copyright © 2017 unrealBots. All rights reserved.
//

import UIKit

class Song{
    var id: String
    var image: URL
    var name: String
    var artist: String
    var popularity: Int
    var previewURL: String?
    var spotifyURL: String?
    var artistID: String?
    var songURL: URL?
    
    init(songId id: String, songImage image: URL, songName name: String, songArtist artist: String,
         songPopularity popularity: Int, songPreviewURL previewURL: String?, spotifyURL: String? = nil,
         artistID: String? = nil, songURL: URL? = nil){
        self.id = id
        self.image = image
        self.name = name
        self.artist = artist
        self.popularity = popularity
        self.previewURL = previewURL
        self.spotifyURL = spotifyURL
        self.artistID = artistID
        self.songURL = songURL
    }
    
    convenience init(songImage image: URL, songName name: String, songArtist artist: String,
                     songPopularity popularity: Int, songPreviewURL previewURL: String?){
        self.init(songId: "", songImage: image, songName: name, songArtist: artist, songPopularity: popularity,
                  songPreviewURL: previewURL, spotifyURL: nil, artistID: nil)
    }
    
    convenience init(songArtist artist: String, songImage image: URL, songName name: String,
                     spotifyURL: String? = nil, artistID: String? = nil, songURL: URL? = nil){
        self.init(songId: "", songImage: image, songName: name, songArtist: artist, songPopularity: 0, songPreviewURL: nil,
                  spotifyURL: spotifyURL, artistID: artistID, songURL: songURL)
    }
    
    convenience init(songName: String, songImage: URL) {
        self.init(songId: "", songImage: songImage, songName: songName, songArtist: "", songPopularity: 0, songPreviewURL: nil)
    }
}
