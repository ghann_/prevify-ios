//
//  UserMenuLeftTableViewCell.swift
//  prevify
//
//  Created by unrealBots on 6/9/17.
//  Copyright © 2017 unrealBots. All rights reserved.
//

import UIKit

class UserMenuLeftTableViewCell: UITableViewCell {

    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var usernameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

}
