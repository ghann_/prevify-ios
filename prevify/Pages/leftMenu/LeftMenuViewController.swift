//
//  LeftMenuViewController.swift
//  prevify
//
//  Created by unrealBots on 6/6/17.
//  Copyright © 2017 unrealBots. All rights reserved.
//

import UIKit
import HexColors

class LeftMenuViewController: UIViewController{
    
    let defaults: UserDefaults = UserDefaults.standard
    var username = ""
    
    @IBOutlet weak var tableView: UITableView!
}


// MARK: - Life Cycle
extension LeftMenuViewController{
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        if self.defaults.bool(forKey: "isLoggedIn")  == true{
            username = self.defaults.string(forKey: "username")!
            tableView.reloadData()
        }
    }
}


// MARK: - UITableViewDataSource
extension LeftMenuViewController: UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Globals.leftMenuItem.count + 1
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: UITableViewCell
        
        switch indexPath.row {
        case 0:
            let userCell = tableView.dequeueReusableCell(withIdentifier: "userCell") as! UserMenuLeftTableViewCell
            userCell.profileImage.layer.cornerRadius = 8.0
            userCell.profileImage.clipsToBounds = true
            userCell.usernameLabel.text = username
            
            cell = userCell
        default:
            let menuCell = tableView.dequeueReusableCell(withIdentifier: "menuCell") as! LeftMenuTableViewCell
            menuCell.menuName?.text  = Globals.leftMenuItem[indexPath.row-1]
            menuCell.iconMenu?.image = UIImage(named:  Globals.leftMenuIcon[indexPath.row-1])
            
            cell = menuCell
        }
        
        cell.selectionStyle  = .none
        cell.backgroundColor = UIColor("#f9fffb")
        
        return cell
    }
}


// MARK: - UITableViewDelegate
extension LeftMenuViewController: UITableViewDelegate{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.row {
            case 0: return 60
            default: return 45
        }
    }
    func tableView(_ tableView: UITableView, didHighlightRowAt indexPath: IndexPath) {
        switch indexPath.row {
        case 0: break
        default:
            let cell = tableView.cellForRow(at: indexPath)
            cell?.contentView.backgroundColor = UIColor("aae0c8")
        }
    }
    func tableView(_ tableView: UITableView, didUnhighlightRowAt indexPath: IndexPath) {
        switch indexPath.row {
        case 0: break
        default:
            let cell = tableView.cellForRow(at: indexPath)
            cell?.contentView.backgroundColor = UIColor("#f9fffb")
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        var vc: UIViewController? = nil
        var loggedOut = false
        
        switch indexPath.row{
            case 0:
                return
            case 1:
                vc = self.storyboard?.instantiateViewController(withIdentifier: "goToHome")
                break
            case 2:
                vc = self.storyboard?.instantiateViewController(withIdentifier: "goToSearch")
                break
            case 3:
                vc = self.storyboard?.instantiateViewController(withIdentifier: "goToLiked")
                break
            case 4:
                vc = self.storyboard?.instantiateViewController(withIdentifier: "goToAbout")
                break
            case 5:
                self.defaults.set(false, forKey: "isLoggedIn")
                let domain = Bundle.main.bundleIdentifier!
                UserDefaults.standard.removePersistentDomain(forName: domain)
                UserDefaults.standard.synchronize()
                loggedOut = true
                
                vc = self.storyboard?.instantiateViewController(withIdentifier: "goToSignInVC")
                break
            default: break
        }
        
        if let container = self.so_containerViewController {
            container.isSideViewControllerPresented = false
        }
        DispatchQueue.main.async {
            if loggedOut{
                self.present(vc!, animated: true, completion: nil)
            }else{
                self.so_containerViewController?.topViewController = vc
            }
        }
    }
}
