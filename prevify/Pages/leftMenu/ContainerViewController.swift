//
//  HomeViewController.swift
//  prevify
//
//  Created by unrealBots on 6/6/17.
//  Copyright © 2017 unrealBots. All rights reserved.
//

import UIKit
import SidebarOverlay

class ContainerViewController: SOContainerViewController {
    
    var shortcut: Globals.Shortcut? = nil
    
    func handleQuickAction(shortcut: Globals.Shortcut?){
        guard let type = shortcut else { configureSideMenu(); return}
        switch type {
        case .Search:
            let search = UIStoryboard(name: "Search", bundle: nil)
            configureSideMenu(with: search)
        case .Liked:
            let search = UIStoryboard(name: "Liked", bundle: nil)
            configureSideMenu(with: search)
        }
    }
    
    func configureSideMenu(with storyboard: UIStoryboard = UIStoryboard(name: "Home", bundle: nil)){
        self.topViewController = storyboard.instantiateInitialViewController()
        self.menuSide = .left
        self.sideViewController = self.storyboard?.instantiateViewController(withIdentifier: "leftScreen")
        self.sideMenuWidth = 75 * self.view.bounds.width / 100
        if let container = self.so_containerViewController {
            container.isSideViewControllerPresented = false
        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
}

// MARK: - Life Cycle
extension ContainerViewController{
    override func viewDidLoad() {
        super.viewDidLoad()
        
        handleQuickAction(shortcut: shortcut)
    }
}
