//
//  LikedListViewController.swift
//  prevify
//
//  Created by unrealBots on 6/6/17.
//  Copyright © 2017 unrealBots. All rights reserved.
//

import UIKit
import SVProgressHUD

class LikedListViewController: UIViewController {
    
    fileprivate let api = API.shared
    fileprivate let dispatchGroup = DispatchGroup()
    fileprivate var trackidCollection : [String] = []
    fileprivate var songs = [Song]()
    var selectedSong: Song? = nil
    
    @IBOutlet weak var tableView: UITableView!
    @IBAction func trashClicked(sender: UIButton) {
        showDefaultAlert(alertTitle: "Remove Track", alertMessage: "Are you sure want to remove\n\(songs[sender.tag].name)\nfrom liked list?", okText: "Proceed", okHandler: {
            self.removeTrack(song: self.songs[sender.tag])
            self.trackidCollection.remove(at: sender.tag)
            self.songs.remove(at: sender.tag)
            self.trackidCollection.count < 1 ? self.navigationItem.rightBarButtonItems?.removeAll() : nil
        })
    }
    
    func setUpNavigation(){
        setUpNavigationForLiked { leftMenu, centerLogo, trashButton in
            leftMenu.addTarget(self, action: #selector(openMenu), for: .touchUpInside)
            trashButton.addTarget(self, action: #selector(self.btnTrashButtonTapped), for: .touchUpInside)
            
            self.navigationItem.titleView = centerLogo
            let leftBarButton = UIBarButtonItem(customView: leftMenu)
            let rightBarButton = UIBarButtonItem(customView: trashButton)
            self.navigationItem.leftBarButtonItem = leftBarButton
            self.navigationItem.rightBarButtonItem = rightBarButton
        }
    }
    @objc func openMenu(){
        if let container = self.so_containerViewController {
            container.isSideViewControllerPresented = true
        }
    }

    @objc func btnTrashButtonTapped(){
        showDefaultAlert(alertTitle: "Warning", alertMessage: "Are you sure want to remove all songs?", okHandler: {
             self.deleteAllTracks()
        }, cancelHandler: nil)
    }
    func deleteAllTracks(){
        SVProgressHUD.show()
        api.removeAllLikedList { success, response, error in
            SVProgressHUD.dismiss(withDelay: 0.5, completion: {
                if success{
                    guard let response = response as? String else {return}
                    self.showAlertWithOkOnly(alertTitle: "Info", alertMessage: "\(response)", okHandler: {
                        self.navigationItem.rightBarButtonItems?.removeAll()
                        self.trackidCollection.removeAll()
                        self.tableView.reloadData()
                    })
                }else{
                    guard let error = error else {return}
                    print("Error: \(error)")
                }
            })
        }
    }
    func removeTrack(song: Song){
        SVProgressHUD.show()
        api.removeTracks(song: song) { success, response, error in
            SVProgressHUD.dismiss(withDelay: 0.5, completion: {
                if success {
                    guard let response = response else {return}
                    self.showAlertWithOkOnly(alertTitle: "Success", alertMessage: "\(response)", okHandler: {
                        self.tableView.reloadData()
                    })
                }else{
                    guard let error = error else {return}
                    print("Error: \(error)")
                }
            })
        }
    }
    
    func getLikedData(){
        trackidCollection.removeAll()
        songs.removeAll()

        SVProgressHUD.show(withStatus: "Fetching Data")
        dispatchGroup.enter()
        getTrackIds {
            self.api.getLikedList(trackIds: self.trackidCollection) { success, songs, error in
                SVProgressHUD.showSuccess(withStatus: "Fetch success")
                SVProgressHUD.dismiss(withDelay: 0.5, completion: {
                    if success{
                        guard let songs = songs else {return}
                        self.songs = songs
                        
                        self.dispatchGroup.leave()
                        self.dispatchGroup.notify(queue: .main, execute: {
                            self.trackidCollection.count < 1 ? self.navigationItem.rightBarButtonItems?.removeAll() : nil
                            self.tableView.reloadData()
                        })
                    }else{
                        guard let error = error else {return}
                        print("Error: \(error)")
                    }
                })
            }
        }
    }
    
    func getTrackIds(handler: @escaping ()->()){
        api.getLikedListTrackID { success, data, error in
            if success{
                guard let trackIDs = data as? [String] else {return}
                self.trackidCollection = trackIDs
                handler()
            }else{
                SVProgressHUD.showError(withStatus: "Error: \(error!.localizedDescription)")
                SVProgressHUD.dismiss(withDelay: 0.5)
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "goToPreviewSegue"{
            guard let audioVC = segue.destination as? AudioPreviewViewController else {return}
            audioVC.isFromLiked = true
            audioVC.song = selectedSong
        }
    }
}

// MARK: - Life Cycle
extension LikedListViewController{
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setUpNavigation()
        getLikedData()
    }
}

// MARK: - TableView DataSource
extension LikedListViewController: UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        trackidCollection.count > 0 ? configureTableView(tableView: tableView) : configureEmptyTableView(tableView: tableView, emptyText: "No data available")
        return trackidCollection.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "likedCell") as! LikedListTableViewCell
        
        cell.buttonTrash.setImage(UIImage(named: "trash-tapped.png"), for: .highlighted)
        cell.buttonTrash.tag = indexPath.row
        cell.imagePicture.layer.cornerRadius = 3.5
        cell.imagePicture.clipsToBounds = true
        downloadImage(from: songs[indexPath.row].image, to: cell.imagePicture)
        cell.artistName.text = songs[indexPath.row].artist
        cell.songTitle.text = songs[indexPath.row].name
        
        let popularity: Int = songs[indexPath.row].popularity
        cell.star1.image = (popularity > 0  && popularity < 21 ) || popularity > 20 ? UIImage(named: "star") : UIImage(named: "star-white")
        cell.star2.image = (popularity > 20 && popularity < 41 ) || popularity > 40 ? UIImage(named: "star") : UIImage(named: "star-white")
        cell.star3.image = (popularity > 40 && popularity < 61 ) || popularity > 60 ? UIImage(named: "star") : UIImage(named: "star-white")
        cell.star4.image = (popularity > 60 && popularity < 81 ) || popularity > 80 ? UIImage(named: "star") : UIImage(named: "star-white")
        cell.star5.image = (popularity > 80 ) ? UIImage(named: "star") : UIImage(named: "star-white")
        cell.backgroundColor = UIColor("#f9fffb")
        cell.selectionStyle = .none
        
        return cell
    }
}

// MARK: - TableView Delegate
extension LikedListViewController: UITableViewDelegate{
    func tableView(_ tableView: UITableView, didHighlightRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath)
        cell?.contentView.backgroundColor = UIColor("#aae0c8")
    }
    func tableView(_ tableView: UITableView, didUnhighlightRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath)
        cell?.contentView.backgroundColor = UIColor("#f9fffb")
        selectedSong = songs[indexPath.row]
        performSegue(withIdentifier: "goToPreviewSegue", sender: nil)
    }
}

// MARK: - TableViewEmptiable
extension LikedListViewController: TableViewEmptiable {}

// MARK: - ImageCacheable
extension LikedListViewController: ImageCacheable {}
