//
//  SignUpViewController.swift
//  prevify
//
//  Created by unrealBots on 6/6/17.
//  Copyright © 2017 unrealBots. All rights reserved.
//

import UIKit
import SVProgressHUD
import SwiftHash

class SignUpViewController: UIViewController {
    
    fileprivate let api = API.shared
    
    @IBOutlet weak var textfieldUsername: UITextField?
    @IBOutlet weak var textfieldPassword: UITextField?
    @IBAction func backToLogin(_ sender: UIButton!) { dismiss(animated: true, completion: nil) }
    @IBAction func signUpTapped(_ sender: Any) {
        guard let username = textfieldUsername?.text, !username.isEmpty,
              var password = textfieldPassword?.text, !password.isEmpty else{
                showAlertWithOkOnly(alertTitle: "Error", alertMessage: "Please fill up the forms")
                return
        }
        password = MD5("\(password)prevify")
        SVProgressHUD.show()
        registerUser(username: username, password: password)
    }
    
    func registerUser(username: String, password: String){
        api.register(with: username, and: password) { success, responseCode, error in
            SVProgressHUD.dismiss(withDelay: 0.5, completion: {
                if success{
                    if responseCode == 1 {
                        self.showAlertWithOkOnly(alertTitle: "Success", alertMessage: "Sign Up Success"){
                            self.dismiss(animated: true, completion: nil)
                        }
                    }else{
                        self.showAlertWithOkOnly(alertTitle: "Error", alertMessage: "User already exist")
                    }
                }else{
                    guard let error = error else {return}
                    print("Error: \(error.localizedDescription)")
                }
            })
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
}

// MARK: - Life Cycle
extension SignUpViewController{
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.addBackground()
    }
}

// MARK: - TextField Delegate
extension SignUpViewController: UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField.returnKeyType {
        case .next:
            textfieldUsername?.resignFirstResponder()
            textfieldPassword?.becomeFirstResponder()
        case .go:
            textField.resignFirstResponder()
            signUpTapped(self)
        default : ()
        }
        return true
    }
}
