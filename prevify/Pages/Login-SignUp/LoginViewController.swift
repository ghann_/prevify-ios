//
//  ViewController.swift
//  prevify
//
//  Created by unrealBots on 5/31/17.
//  Copyright © 2017 unrealBots. All rights reserved.
//

import UIKit
import SVProgressHUD
import SparkImageView
import SidebarOverlay
import SwiftHash

class LoginViewController: UIViewController {

    var successLogin = false
    let api = API.shared
    
    @IBOutlet weak var viewFooter: UIView?
    @IBOutlet weak var viewInput: UIView?
    @IBOutlet weak var mainlogo: SparkImageView?
    @IBOutlet weak var usernameTextField: UITextField?
    @IBOutlet weak var passwordTextField: UITextField?
    @IBAction func loginClicked(_ sender: Any) {
        guard let username = usernameTextField?.text, !username.isEmpty,
              var password = passwordTextField?.text, !password.isEmpty else{
            showAlertWithOkOnly(alertTitle: "Error", alertMessage: "Please fill up the forms")
            return
        }
        password = MD5("\(password)prevify")
        SVProgressHUD.show()
        goLogin(username: username, password: password)
    }
    @IBAction func logoClicked(_ sender: Any) {
        mainlogo?.animate(withDuration: 10, completion: nil)
    }
    @IBAction func btnGoToAboutClicked(_ sender: UIButton){ goToAbout() }
    
    func goLogin(username: String, password: String){
        api.login(with: username, and: password) { success, response, error in
            SVProgressHUD.dismiss(withDelay: 0.5, completion: {
                if success{
                    guard let response = response else{return}
                    let indexUid = response.index(response.startIndex, offsetBy: 1)
                    let userid = String(response[indexUid...])
                    
                    self.showDefaultAlert(alertTitle: "Info", alertMessage: "Login Success", okText: "Authenticate", okHandler:{
                        self.doOauth();
                        Globals.defaults.set("\(userid)", forKey: "uid")
                        Globals.defaults.set(username, forKey: "username")
                    })
                }else{
                    if let error = error{
                        print("Error: \(error.localizedDescription)")
                        return
                    }
                    guard let response = response else{return}
                    self.showAlertWithOkOnly(alertTitle: "Error", alertMessage: response)
                }
            })
        }
    }
    
    func configureLogo(){
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(logoClicked(_:)))
        mainlogo?.isUserInteractionEnabled = true
        mainlogo?.addGestureRecognizer(tapGestureRecognizer)
        mainlogo?.logoFadeIn {
             self.logoClicked(self)
        }
    }
    func configureInputAndFooter(){
        viewInput?.fadeIn(after: 1.2)
        viewFooter?.fadeIn(after: 1.6)
    }
    
    func goToAbout(){
        let aboutVC = UIStoryboard(name: "About", bundle: nil).instantiateViewController(withIdentifier: "AboutVC") as! AboutViewController
        aboutVC.fromLogin = true
        aboutVC.modalTransitionStyle = .crossDissolve
        present(aboutVC, animated: true, completion: nil)
    }
    func goToHome(){
        let storyboard = UIStoryboard(name: "LeftMenu", bundle: nil)
        let vc = storyboard.instantiateInitialViewController()
        Globals.appDelegate?.window?.rootViewController = vc
    }
    
    func doOauth(){
        api.doOAuth(from: self) { success, token, error in
            if success{
                Globals.defaults.set(token, forKey: "token")
                Globals.defaults.set(true, forKey: "isLoggedIn")
                self.successLogin = true
            }else{
                guard let err = error else {return}
                self.showAlertWithOkOnly(alertTitle: "Error", alertMessage: "Error: \(err.localizedDescription)")
            }
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
}

// MARK: - Life Cycle
extension LoginViewController{
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.addBackground()
        configureLogo()
        configureInputAndFooter()
        Globals.defaults.removeObject(forKey: "token")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        successLogin ? goToHome() : nil
    }
}

// MARK: - TextField Delegate
extension LoginViewController: UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField.returnKeyType {
        case .next:
            usernameTextField?.resignFirstResponder()
            passwordTextField?.becomeFirstResponder()
        case .go:
            view.endEditing(true)
            loginClicked(self)
        default: ()
        }
        return true
    }
}
