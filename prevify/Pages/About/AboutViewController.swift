//
//  AboutViewController.swift
//  prevify
//
//  Created by unrealBots on 6/6/17.
//  Copyright © 2017 unrealBots. All rights reserved.
//

import UIKit
import HexColors

class AboutViewController: UIViewController {

    var fromLogin = false
    
    @IBOutlet weak var footerView: UIView!
    @IBOutlet weak var profileImage: UIImageView!{
        didSet{
            profileImage.layer.cornerRadius = UIScreen.main.bounds.width * 0.55 / 2
            profileImage.layer.borderWidth = 2.0
            profileImage.layer.borderColor = UIColor("#5CDEA2")!.cgColor
            profileImage.clipsToBounds = true
        }
    }
    @IBAction func backToLogin(_ sender: UIButton) { dismiss(animated: true, completion: nil) }

    func setUpNavigation(){
        setUpNavigation { leftMenu, centerLogo in
            leftMenu.addTarget(self, action: #selector(openMenu), for: .touchUpInside)
            
            self.navigationItem.titleView = centerLogo
            let barButton = UIBarButtonItem(customView: leftMenu)
            self.navigationItem.leftBarButtonItem = barButton
        }
    }
    
    @objc func openMenu(){
        if let container = self.so_containerViewController {
            container.isSideViewControllerPresented = true
        }
    }
}

// MARK: - Life Cycle
extension AboutViewController{
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.addBackground()
        setUpNavigation()
        footerView.isHidden = fromLogin ? false : true
    }
}
