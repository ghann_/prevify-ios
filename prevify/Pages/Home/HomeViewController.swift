//
//  HomeViewController.swift
//  prevify
//
//  Created by unrealBots on 6/6/17.
//  Copyright © 2017 unrealBots. All rights reserved.
//

import UIKit
import HexColors
import SVProgressHUD
import LTMorphingLabel

class HomeViewController: UIViewController{
    
    fileprivate let api = API.shared
    fileprivate var newReleasesCollections = [Song]()
    lazy var refresher: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(refresh(sender:)), for: .valueChanged)
        
        return refreshControl
    }()
    
    @IBOutlet weak var collectionView: UICollectionView!{
        didSet{
            collectionView.backgroundView = UIImageView(image: UIImage(named: "background.png"))
        }
    }
    @IBOutlet weak var newReleaseLabel: LTMorphingLabel!
    
    func setUpNavigation(){
        setUpNavigation { leftMenu, centerLogo in
            leftMenu.addTarget(self, action: #selector(openMenu), for: .touchUpInside)
            
            self.navigationItem.titleView = centerLogo
            let barButton = UIBarButtonItem(customView: leftMenu)
            self.navigationItem.leftBarButtonItem = barButton
        }
    }
    
    func setUpAnimationLabel(){
        UIView.animate(withDuration: 1, delay: 0, options: .curveEaseInOut, animations: {
            self.newReleaseLabel.alpha = 1
            self.newReleaseLabel.morphingEnabled  = true
            self.newReleaseLabel.morphingEffect   = .scale
            self.newReleaseLabel.morphingDuration = 0.25
            self.newReleaseLabel.morphingCharacterDelay = 0.5
            self.newReleaseLabel.text = "New Releases"
        }, completion: nil)
    }
    func setUpRefreshControl(){
        collectionView.refreshControl = refresher
    }
    
    @objc func refresh(sender: UIRefreshControl){
        newReleasesCollections.removeAll()
        getNewReleases{
            let deadline = DispatchTime.now() + .milliseconds(1000)
            DispatchQueue.main.asyncAfter(deadline: deadline){
                self.refresher.endRefreshing()
            }
        }
    }
    @objc func openMenu(){
        if let container = self.so_containerViewController {
            container.isSideViewControllerPresented = true
        }
    }
    
    func getNewReleases(handler: (()->())? = nil){
        api.getNewReleases{ success, data, error in
            if success{
                guard let newReleasesCollections = data as? [Song] else {return}
                self.newReleasesCollections = newReleasesCollections
                self.setUpAnimationLabel()
                self.collectionView.reloadData()
                handler?()
            }else{
                guard let error = error else{
                    self.showDefaultAlert(alertTitle: "Error", alertMessage: "\(data as! String)", okText: "ReAuthenticate", okHandler: { 
                        self.api.logoutAuth{
                            let vc = UIStoryboard(name: "LoginSignUp", bundle: nil).instantiateInitialViewController()
                            self.present(vc!, animated: true, completion: nil)
                        }
                    }, cancelHandler: nil)
                    return
                }
                print("Error: \(error)")
            }
        }
    }
    
    func checkConnection(){
        if Reachability.isConnectedToNetwork() {
            DispatchQueue.main.async {
                self.getNewReleases{
                    self.collectionView.fadeIn(after: 0.2)
                }
            }
        }else{ showAlertWithOkOnly(alertTitle: "Error", alertMessage: "No Connection Available") }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "goToPreview"{
            guard let searchVC = segue.destination as? SearchTrackTableViewController,
                  let searchFromHome = sender as? String else {return}
            searchVC.searchFromHome = searchFromHome
        }
    }
}

// MARK: - Life Cycle
extension HomeViewController{
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.addBackground()
        setUpNavigation()
        setUpRefreshControl()
        checkConnection()
    }
}

// MARK: - CollectionView Data Source
extension HomeViewController: UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return newReleasesCollections.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "col_cell", for: indexPath) as! HomeCollectionViewCell
        if newReleasesCollections.count>1{
            downloadImage(from: newReleasesCollections[indexPath.row].image, to: cell.mainImage)
            cell.artistLabel.text = newReleasesCollections[indexPath.row].artist
            cell.nameLabel.text   = newReleasesCollections[indexPath.row].name
        }
        
        cell.backgroundColor = UIColor("#f9fffb")
        return cell
    }
}

// MARK: - CollectionView Delegate
extension HomeViewController: UICollectionViewDelegate{
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let cell = collectionView.cellForItem(at: indexPath) as? HomeCollectionViewCell,
              let artist = cell.artistLabel.text,
              let song = cell.nameLabel.text else {return}
        
        showActionSheet(title: artist, message: song, firstActionTitle: "Search Artist", firstHandler: {
            self.performSegue(withIdentifier: "goToPreview", sender: artist)
        }, secondActionTitle: "Search Album", secondHandler: {
            self.performSegue(withIdentifier: "goToPreview", sender: song)
        }, thirdActionTitle: "Open album in Spotify") {
            guard let spotifyUrl = self.newReleasesCollections[indexPath.row].spotifyURL,
                  let url = URL(string: spotifyUrl) else {return}
            self.api.openSpotify(with: url, from: self)
        }
    }
}

// MARK: - CollectionView FlowLayout Delegate
extension HomeViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let padding: CGFloat = 0
        let collectionCellSize = collectionView.frame.size.width - padding
        
        return CGSize(width: collectionCellSize/2 - 28, height: collectionCellSize/2)
    }
}

// MARK: - ImageCacheable
extension HomeViewController: ImageCacheable {}
