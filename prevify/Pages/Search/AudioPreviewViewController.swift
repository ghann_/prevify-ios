//
//  AudioPreviewViewController.swift
//  prevify
//
//  Created by unrealBots on 6/2/17.
//  Copyright © 2017 unrealBots. All rights reserved.
//

import UIKit
import Foundation
import GlitchLabel
import SparkImageView
import LTMorphingLabel
import SVProgressHUD
import Pulsator

class AudioPreviewViewController: UIViewController {

    let api = API.shared
    let audioPlayer = AudioPlayer.shared
    
    var isFromLiked = false
    var song:Song? = nil{
        didSet{
            title = song?.name
        }
    }
    
    @IBOutlet weak var pulsatorView: UIView!{
        didSet{
            let pulsator = Pulsator()
            pulsator.numPulse = 3
            pulsator.radius = 185.0
            pulsator.start()
            pulsator.backgroundColor = UIColor(red:0.67, green:0.88, blue:0.78, alpha:1.0).cgColor
            pulsator.repeatCount = 1000
            pulsator.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
            pulsator.frame = pulsatorView.bounds
            pulsatorView.layer.addSublayer(pulsator)
        }
    }
    @IBOutlet weak var backgroundImage: UIImageView!{
        didSet{
            downloadImage(from: song!.image, to: backgroundImage)
        }
    }
    @IBOutlet weak var songImage: SparkImageView!{
        didSet{
            songImage.layer.cornerRadius = UIScreen.main.bounds.width * 0.688 / 2
            songImage.layer.borderWidth  = 3.0
            songImage.clipsToBounds      = true
            let greenCircle = UIColor(red: 36/255.0, green: 221/255.0, blue: 102/255.0, alpha: 1.0)
            songImage.layer.borderColor = greenCircle.cgColor
            downloadImage(from: song!.image, to: songImage)
            songImage.rotate360Degrees()
            songImage.popRotatingImage()
        }
    }
    @IBOutlet var artistName: LTMorphingLabel!{
        didSet{
            artistName.morphingEnabled  = true
            artistName.morphingDuration = 0.65
            artistName.morphingCharacterDelay = 1.0
            artistName.morphingEffect   = .evaporate
            artistName.morphingProgress = 1.0
            artistName.text = song?.artist
            
            let tapGesture = UITapGestureRecognizer(target: self, action: #selector(artistNameTapped(sender:)))
            artistName.isUserInteractionEnabled = true
            artistName.addGestureRecognizer(tapGesture)
        }
    }
    @IBOutlet weak var songTitle: UILabel!{
        didSet{
            songTitle.text = song?.name
        }
    }
    @IBOutlet weak var spotifyButton: UIButton!
    @IBOutlet weak var playPauseButton: UIButton!
    @IBOutlet weak var playPauseConstraint: NSLayoutConstraint!
    @IBOutlet weak var loveButton: SparkImageView!
    @IBOutlet weak var loveButtonConstraint: NSLayoutConstraint!
    @IBAction func loveClicked(_ sender: Any) {
        loveButton.animate(withDuration: 1, completion: nil)
        
        guard let songName = song?.name else {return}
        showDefaultAlert(alertTitle: "Save", alertMessage: "Save \(songName) into Liked List?", okText: "Save", okHandler:{
            SVProgressHUD.show()
            self.saveToLiked()
        })
    }
    @IBAction func pausePreview(_ sender: Any) {
        if audioPlayer.isPlaying{
            audioPlayer.pause()
            playPauseButton.setTitle("Play Preview", for: .normal)
        }else{
            audioPlayer.play()
            playPauseButton.setTitle("Pause Preview", for: .normal)
        }
    }
    @IBAction func spotifyBtnTapped(_ sender: UIButton!){
        sender.popFadeIn {
            guard let url = self.song?.songURL else {return}
            self.api.openSpotify(with: url, from: self)
        }
    }
    
    func downloadFileFromURL(url: URL){
        var downloadTask = URLSessionDownloadTask()
        downloadTask = URLSession.shared.downloadTask(with: url, completionHandler: {
            customURL, response, error in
            guard let url = customURL else {return}
            self.audioPlayer.delegate = self
            self.audioPlayer.playPreview(of: url)
        })
        downloadTask.resume()
    }
    func saveToLiked(){
        guard let id = song?.id, let name = song?.name else{return}
        api.saveToLikedList(songId: id, songTitle: name) { success, response, error in
            SVProgressHUD.dismiss(withDelay: 0.5, completion: {
                if success{
                    guard let response = response else{return}
                    self.showAlertWithOkOnly(alertTitle: "Info", alertMessage: "\(response)")
                }else{
                    guard let error = error else{return}
                    print("Error: \(error)")
                }
            })
        }
    }
    func setUpPlayer(){
        guard let previewURL = song?.previewURL else {return}
        playPauseButton.setTitle("Pause Preview", for: .normal)
        downloadFileFromURL(url: URL(string: previewURL)!)
    }
    func setUpPlayLoveButton(){
        loveButton.isHidden = isFromLiked ? true : false
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(loveClicked(_:)))
        loveButton.addGestureRecognizer(tapGestureRecognizer)
        loveButton.isUserInteractionEnabled = true
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.playPauseConstraint.constant = 0
            UIView.animate(withDuration: 0.8, delay: 0.0, options: .curveEaseInOut, animations: {
                self.view.layoutIfNeeded()
            })
            self.spotifyButton.fadeIn()
            self.loveButtonConstraint.constant = 0
            UIView.animate(withDuration: 0.65, delay: 0.5, options: .curveEaseInOut, animations: {
                self.view.layoutIfNeeded()
            })
        }
    }
    
    @objc func artistNameTapped(sender: UITapGestureRecognizer? = nil){
        artistName.popFadeIn {
            self.performSegue(withIdentifier: "DetailArtistSegue", sender: nil)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "DetailArtistSegue"{
            guard let detailArtistVC = segue.destination as? DetailArtistViewController else {return}
            detailArtistVC.artistID = song?.artistID
        }
    }
}

// MARK: - Life Cycle
extension AudioPreviewViewController{
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        setUpPlayer()
        setUpPlayLoveButton()
    }
    override func viewWillDisappear(_ animated: Bool) {
        audioPlayer.player.stop()
    }
}

// MARK: - AudioPlayer Delegate
extension AudioPreviewViewController: AudioPlayerDelegate{
    func didFinishedPlayMusic() {
        playPauseButton.setTitle("Play Preview", for: .normal)
    }
}

// MARK: - ImageCacheable Protocol
extension AudioPreviewViewController: ImageCacheable{}
