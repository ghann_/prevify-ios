//
//  SearchTrackTableViewController.swift
//  prevify
//
//  Created by unrealBots on 6/2/17.
//  Copyright © 2017 unrealBots. All rights reserved.
//

import UIKit
import SVProgressHUD
import HexColors

class SearchTrackTableViewController: UIViewController{
    
    fileprivate let api = API.shared
    fileprivate var songs = [Song]()
    var selectedSong: Song? = nil
    var searchFromHome = ""
    
    @IBOutlet weak var searchBar: UISearchBar!{
        didSet{
            searchBar.configureSearchBar()
            if !searchFromHome.isEmpty{
                searchBar.text = searchFromHome
                searchBarSearchButtonClicked(searchBar)
                searchFromHome = ""
            }else{
                searchBar.becomeFirstResponder()
            }
        }
    }
    @IBOutlet weak var tableView: UITableView!
    
    func setUpNavigation(){
        setUpNavigation { leftMenu, centerLogo in
            leftMenu.addTarget(self, action: #selector(openMenu), for: .touchUpInside)
            
            self.navigationItem.titleView = centerLogo
            let barButton = UIBarButtonItem(customView: leftMenu)
            self.navigationItem.leftBarButtonItem = barButton
        }
    }
    @objc func openMenu(){
        if let container = self.so_containerViewController {
            container.isSideViewControllerPresented = true
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "goToPreviewSegue" {
            guard let selectedSong = selectedSong,
                  let audioVC = segue.destination as? AudioPreviewViewController,
                  selectedSong.previewURL != nil else {
                showAlertWithOkOnly(alertTitle: "Error", alertMessage: "Preview URL Not Found")
                return
            }
            audioVC.song = selectedSong
        }
    }
}

// MARK: - Life Cycle
extension SearchTrackTableViewController{
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setUpNavigation()
    }
}

// MARK: - TableView DataSource
extension SearchTrackTableViewController: UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        songs.count > 0 ? configureTableView(tableView: tableView) : configureEmptyTableView(tableView: tableView, emptyText: "No data available")
        return songs.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell") as! CellInfoTableViewCell
        
        cell.mainImageView.layer.cornerRadius = 3.5
        cell.mainImageView.clipsToBounds = true
        
        downloadImage(from: songs[indexPath.row].image, to: cell.mainImageView)
        cell.mainLabel.text      = songs[indexPath.row].name
        cell.artist.text         = songs[indexPath.row].artist
        cell.warning.isHidden    = songs[indexPath.row].previewURL == nil ?  false : true
        
        let popularity = songs[indexPath.row].popularity
        cell.star1.image = (popularity > 0  && popularity < 21 ) || popularity > 20 ? UIImage(named: "star") : UIImage(named: "star-white")
        cell.star2.image = (popularity > 20 && popularity < 41 ) || popularity > 40 ? UIImage(named: "star") : UIImage(named: "star-white")
        cell.star3.image = (popularity > 40 && popularity < 61 ) || popularity > 60 ? UIImage(named: "star") : UIImage(named: "star-white")
        cell.star4.image = (popularity > 60 && popularity < 81 ) || popularity > 80 ? UIImage(named: "star") : UIImage(named: "star-white")
        cell.star5.image = (popularity > 80 ) ? UIImage(named: "star") : UIImage(named: "star-white")
        
        cell.backgroundColor = UIColor("#f9fffb")
        cell.selectionStyle = .none
        
        return cell
    }
}
    
// MARK: - TableView Delegate
extension SearchTrackTableViewController: UITableViewDelegate{
    func tableView(_ tableView: UITableView, didHighlightRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath)
        cell?.contentView.backgroundColor = UIColor("#aae0c8")
    }
    func tableView(_ tableView: UITableView, didUnhighlightRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath)
        cell?.contentView.backgroundColor = UIColor("#f9fffb")
        selectedSong = songs[indexPath.row]
        performSegue(withIdentifier: "goToPreviewSegue", sender: nil)
    }
}

// MARK: - Searchbar Delegate
extension SearchTrackTableViewController: UISearchBarDelegate{
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        guard let keywords = searchBar.text, !keywords.isEmpty else{
            showAlertWithOkOnly(alertTitle: "Error", alertMessage: "Please type some song title")
            return
        }
        SVProgressHUD.show(withStatus: "Fetching Data")
        songs.removeAll()
        
        let finalKeywords = keywords.replacingOccurrences(of: " ", with: "+")
        if searchFromHome.isEmpty { self.view.endEditing(true) }
        
        api.searchTracks(keywords: finalKeywords) { success, data, error in
            SVProgressHUD.showSuccess(withStatus: "Fetching Done")
            SVProgressHUD.dismiss(withDelay: 0.5, completion: {
                if success{
                    guard let songs = data as? [Song] else{return}
                    self.songs = songs
                    self.tableView.reloadData()
                    self.animateTable(table: self.tableView)
                }else{
                    guard let error = error else{
                        self.showDefaultAlert(alertTitle: "Error", alertMessage: "\(data as! String)", okText: "ReAuthenticate", okHandler: {
                            self.api.logoutAuth{
                                let vc = UIStoryboard(name: "LoginSignUp", bundle: nil).instantiateInitialViewController()
                                self.present(vc!, animated: true, completion: nil)
                            }
                        }, cancelHandler: nil)
                        return
                    }
                    print("Error: \(error)")
                }
            })
        }
    }
}

// MARK: - TableViewEmptiable
extension SearchTrackTableViewController: TableViewEmptiable {}

// MARK: - TableViewAnimateable
extension SearchTrackTableViewController: TableViewAnimateable {}

// MARK: - ImageCacheable Protocol
extension SearchTrackTableViewController: ImageCacheable {}
