//
//  DetailArtistViewController.swift
//  prevify
//
//  Created by unrealBots on 2/7/18.
//  Copyright © 2018 unrealBots. All rights reserved.
//

import UIKit

class DetailArtistViewController: UIViewController {
    
    let api = API.shared
    var artistID:String? = nil
    var artist:Artist? = nil
    var topTracks:[Song] = []
    
    @IBOutlet weak var collectionViewTopTracks: UICollectionView!
    @IBOutlet weak var lblTopTracks: UILabel!
    @IBOutlet weak var lblArtistName: UILabel!
    @IBOutlet weak var lblGenres: UILabel!
    @IBOutlet var imgStars: [UIImageView]!
    @IBOutlet weak var lblPopularity: UILabel!
    @IBOutlet weak var lblFollowers: UILabel!
    @IBOutlet weak var containerView: UIView!{
        didSet{
            containerView.layer.cornerRadius = 3.5 * containerView.bounds.width / 100
        }
    }
    @IBOutlet weak var artistImage: UIImageView!{
        didSet{
            let mask = CAGradientLayer()
            mask.colors = [UIColor.black.cgColor, UIColor.clear.cgColor]
            mask.locations =  [0, 0.9]
            mask.frame = artistImage.bounds
            artistImage.layer.mask = mask
            artistImage.applyMotionEffect(magnitude: 20)
        }
    }
    @IBAction func btnCloseTapped(_ sender: UIButton!){
        sender.popFadeIn {
            self.dismiss(animated: true, completion: nil)
        }
    }
    @IBAction func openArtistInSpotify(_ sendeliner: UIButton!){
        guard let artistURL = artist?.spotifyURL,
              let url = URL(string: artistURL) else {return}
        api.openSpotify(with: url, from: self)
    }
    
    func getDetailArtist(handler: @escaping (Artist)->Void){
        guard let artistID = artistID else {return}
        api.getDetailArtist(artistID: artistID) { success, data, error in
            if success{
                guard let artist = data as? Artist else {return}
                handler(artist)
            }else{
                guard let error = error else {return}
                print("Error: \(error)")
            }
        }
    }
    
    func configureDedtailArtistView(){
        getDetailArtist { artist in
            self.artist = artist
            self.downloadImage(from: artist.image, to: self.artistImage)
            self.lblArtistName.text = artist.name
            self.lblTopTracks.text = "Top tracks from \(artist.name)"
            self.lblPopularity.text = "\(artist.popularity) out of 100"
            self.lblFollowers.text = "\(artist.followers) Followers"
            self.lblGenres.text = "\(artist.genre.capitalized)"
            
            var idx = 0
            for imageView in self.imgStars{
                imageView.image = artist.popularity > idx ? UIImage(named: "star") : UIImage(named: "star-white")
                idx += 20
            }
        }
    }
    
    func configureTopTracks(){
        self.api.getTopTracksFromArtist(artistID: self.artistID!) { success, data, error in
            if success{
                guard let topTracks = data as? [Song] else {return}
                DispatchQueue.main.async {
                    self.topTracks = topTracks
                    self.collectionViewTopTracks.reloadData()
                }
            }else{
                guard let err = error else {return}
                print("Error: \(err.localizedDescription)")
            }
        }
    }
}

// MARK: - Life Cycle
extension DetailArtistViewController{
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        configureDedtailArtistView()
        configureTopTracks()
    }
}

// MARK: - CollectionView DataSource
extension DetailArtistViewController: UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return topTracks.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TopTracksCell", for: indexPath) as? TopTracksCell else {return UICollectionViewCell()}
        downloadImage(from: topTracks[indexPath.row].image, to: cell.trackImage)
        cell.lblSongName.text =  topTracks[indexPath.row].name
        return cell
    }
}

// MARK: - CollectionView FlowLayout Delegate
extension DetailArtistViewController: UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let padding: CGFloat = 10
        let collectionCellSize = collectionView.frame.size.width - padding
        
        return CGSize(width: collectionCellSize/3 - 10, height: collectionCellSize/3 - 10)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsetsMake(0, 10, 0, 10)
    }
}

// MARK: - ImageCacheable Protocol
extension DetailArtistViewController: ImageCacheable {}
