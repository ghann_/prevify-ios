//
//  TopTracksCell.swift
//  prevify
//
//  Created by unrealBots on 2/15/18.
//  Copyright © 2018 unrealBots. All rights reserved.
//

import UIKit

class TopTracksCell: UICollectionViewCell {
    
    @IBOutlet weak var trackImage: UIImageView!
    @IBOutlet weak var lblSongName: UILabel!
}
